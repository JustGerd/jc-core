lazy val jc_core = (project in file(".")).settings(
  organization := "tech.gerd",
  scalaVersion := "2.12.4",
  version := "0.1.0",
  name := "jc-core",
  libraryDependencies ++= Seq(
    "org.postgresql" % "postgresql" % "9.4.1208",
    "io.getquill" %% "quill-jdbc" % "2.5.4",
    "org.scalatra" %% "scalatra" % "2.6.+"
  )
)