# jc-core Design Documents

This document describes the goals that I'm trying to achieve in extracting the jc-core functionality from the main jose project.

## Goals
1. Cleaner Code
2. Closer to a real economy

### Real Economy

I intend to add a few features that move jc closer to a real economy. This will be achieved through taxes, loans, a central bank and interest.

#### Central Bank

It is vital to have a central bank in every economy, both to provide a permanent inflow of money to banks and to regulate the general flow of money. In jc, currently taxbanks in each guild fulfill this role. The issue obviously is that there's way to many tax banks, and that the distribution of power shifts based on guild wealth. A central bank would fix these issues.

The central bank will be implemented by a separate fake account (similar to taxbanks) which starts out with no money. Every [time unit] this central bank "mints" a fixed amount of money that then gets introduced into the system. This minted money then gets distributed to the taxbanks, which then continue distribution based on existing rules. Taxes no longer go solely to the taxbanks, but also partially to the central bank. At every "minting session", all of the money that the central bank contains will be distributed to the taxbanks, not only the newly minted money. This is comparable with social benefits. 

#### Taxes

Taxes are already implemented in jc, but they are not a part of jc-core, as they are intended to provide an easy way to ratelimit jose users on commands that call external APIs. To move jc closer to a real economy, the tax system needs an overhaul. Already, taxes are based on the wealth of a user (read: size of savings). This will be continued, but a few new taxes will be added.

###### 1. Income Tax

Income tax is an integral part of every economy. Based on your income, you need to give a certain amount of money to the state (in the case of jose, the taxbanks and the central bank). 
